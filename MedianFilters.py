import cv2
import numpy as np
class MedianFilters:
	def __init__(self, img, dimension):
		self.img = img
		self.dimension = dimension

	def create_padded(self, ch):

		dim = self.dimension // 2	

		# print(ch)
		padded = np.zeros((ch.shape[0] + 2*(dim), ch.shape[1] + 2*(dim)))
		# print("padded shape:", padded.shape)
		# print("ch shape:", ch.shape)
		
		#FILLING THE CORNERS with the corner values of the relative channel of the image
		#top corner(s)
		for i in range(dim):
			#left corners
			for j in range(dim):
				padded[i, j] = ch[0,0]
			#right corners
			for k in range(padded.shape[1] - dim, padded.shape[1]):
				padded[i, k] = ch[0, ch.shape[1] - 1]

		#bottom corner(s)
		for i in range(padded.shape[0] - dim, padded.shape[0]):
			#left
			for j in range(dim):
				padded[i, j] = ch[ch.shape[0] - 1, 0]
			#right
			for k in range(padded.shape[1] - dim, padded.shape[1]):
				padded[i, k] = ch[ch.shape[0] - 1, ch.shape[1] - 1]

		#FILLING THE EDGES
		for k in range(dim, padded.shape[1] - dim):  #iterate over columns first
			#top rows
			for m in range(dim):
				padded[m, k] = ch[0, k - dim]
			#bottom rows
			for p in range(padded.shape[0] - dim, padded.shape[0]):
				padded[p, k] = ch[ch.shape[0] - 1, k - dim]


		#the most 
		for k in range(dim, padded.shape[0] - dim):
			#right row(s)
			for m in range(padded.shape[1] - dim, padded.shape[1]):
				padded[k, m] = ch[k - dim, ch.shape[1] - 1]
			#left row(s)
			for p in range(dim):
				padded[k, p] = ch[k - dim, 0]
			#the overlapped part of padded and the actual image
			for c in range(dim, padded.shape[1] - dim):
				padded[k, c] = ch[k - dim, c - dim] 

		return padded

	def response_ch(self, padded, ch):
		res_ch = np.zeros((ch.shape[0], ch.shape[1]))
		median_arr = np.zeros((self.dimension*self.dimension))
		index = 0
		total = 0 
		avg = 0 
		row = 0
		col = 0
		for x in range(padded.shape[0] - self.dimension + 1):
			for y in range(padded.shape[1] - self.dimension + 1):
				for i in range(self.dimension):
					for j in range(self.dimension):
						val = padded[x + i, y + j] #Put the values where image and filter is overlapped into an array
						median_arr[index] = val
						index += 1
						

				median_arr.sort()
				index = 0
				res_ch[row, col] = median_arr[(self.dimension*self.dimension) // 2] #sortlanmış arrayin medyanını ata
				col += 1

			col = 0
			row += 1

		return res_ch

	def median_filter(self):
		#her bir channel'ı ayır ve average işlemini yap. 
		#her bir channel'ı numpy array olarak döndür ve o numpy array npuint8 ve dstack ile resim haline gelecek.
		#odev1'e bak bunun için
		#print(self.img.shape)
		b, g, r = cv2.split(self.img)
		
		#print(g.shape) #(427, 277)
		#print(b[0, 0]) #37

		padded_b = self.create_padded(b)
		padded_g = self.create_padded(g)
		padded_r = self.create_padded(r)

		b = self.response_ch(padded_b, b)
		g = self.response_ch(padded_g, g)
		r = self.response_ch(padded_r, r)
		
		b = np.uint8(b)
		g = np.uint8(g)
		r = np.uint8(r)
		image = cv2.merge((b, g, r))
		print(image.shape)
		ext = str(self.dimension)
		cv2.imwrite("median" + ext + ".jpg", image)

		# print("padded")
		# print(padded_b)

		
		



		
		

		


# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!
import sys
import cv2
import numpy as np
from AverageFilters import AverageFilters
from GaussianFilters import GaussianFilters
from MedianFilters import MedianFilters
from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(565, 453)
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralWidget)
        self.horizontalLayout.setContentsMargins(11, 11, 11, 11)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(6)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        
        self.image = QtWidgets.QLabel(self.centralWidget)
        self.image.setText("")
        self.image.setObjectName("image")
        
        self.horizontalLayout_2.addWidget(self.image)
        self.horizontalLayout.addLayout(self.horizontalLayout_2)
        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QtWidgets.QMenuBar(MainWindow) 
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 565, 22))
        self.menuBar.setNativeMenuBar(False)
        self.menuBar.setObjectName("menuBar")
        self.menuFile = QtWidgets.QMenu(self.menuBar)
        self.menuFile.setObjectName("menuFile")
        self.menuFilters = QtWidgets.QMenu(self.menuBar)
        self.menuFilters.setObjectName("menuFilters")
        self.menuAverage_Filters = QtWidgets.QMenu(self.menuFilters)
        self.menuAverage_Filters.setObjectName("menuAverage_Filters")
        self.menuGaussian_Filters = QtWidgets.QMenu(self.menuFilters)
        self.menuGaussian_Filters.setObjectName("menuGaussian_Filters")
        self.menuMedian_Filters = QtWidgets.QMenu(self.menuFilters)
        self.menuMedian_Filters.setObjectName("menuMedian_Filters")
        self.menuGeometric_Transforms = QtWidgets.QMenu(self.menuBar)
        self.menuGeometric_Transforms.setObjectName("menuGeometric_Transforms")
        self.menuRotate = QtWidgets.QMenu(self.menuGeometric_Transforms)
        self.menuRotate.setObjectName("menuRotate")
        self.menuScale = QtWidgets.QMenu(self.menuGeometric_Transforms)
        self.menuScale.setObjectName("menuScale")
        self.menuTranslate = QtWidgets.QMenu(self.menuGeometric_Transforms)
        self.menuTranslate.setObjectName("menuTranslate")
        MainWindow.setMenuBar(self.menuBar)
        self.mainToolBar = QtWidgets.QToolBar(MainWindow)
        self.mainToolBar.setObjectName("mainToolBar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QtWidgets.QStatusBar(MainWindow)
        self.statusBar.setObjectName("statusBar")
        MainWindow.setStatusBar(self.statusBar)
        self.actionSave = QtWidgets.QAction(MainWindow)
        self.actionSave.setObjectName("actionSave")
        
        self.actionOpen = QtWidgets.QAction(MainWindow)
        self.actionOpen.triggered.connect(self.on_clicked_open)
        self.actionOpen.setObjectName("actionOpen")
        
        self.actionSave_2 = QtWidgets.QAction(MainWindow)
        self.actionSave_2.setObjectName("actionSave_2")
        
        self.actionExit = QtWidgets.QAction(MainWindow)
        self.actionExit.setObjectName("actionExit")
        
        self.average3x3 = QtWidgets.QAction(MainWindow)
        self.average3x3.triggered.connect(self.on_clicked_avg3)
        self.average3x3.setObjectName("average3x3")
        
        self.average5x5 = QtWidgets.QAction(MainWindow)
        self.average5x5.triggered.connect(self.on_clicked_avg5)
        self.average5x5.setObjectName("average5x5")
        
        self.average7x7 = QtWidgets.QAction(MainWindow)
        self.average7x7.triggered.connect(self.on_clicked_avg7)
        self.average7x7.setObjectName("average7x7")
        
        self.average9x9 = QtWidgets.QAction(MainWindow)
        self.average9x9.triggered.connect(self.on_clicked_avg9)
        self.average9x9.setObjectName("average9x9")
        
        self.average11x11 = QtWidgets.QAction(MainWindow)
        self.average11x11.triggered.connect(self.on_clicked_avg11)
        self.average11x11.setObjectName("average11x11")
        
        self.average13x13 = QtWidgets.QAction(MainWindow)
        self.average13x13.triggered.connect(self.on_clicked_avg13)
        self.average13x13.setObjectName("average13x13")
        
        self.average15x15 = QtWidgets.QAction(MainWindow)
        self.average15x15.triggered.connect(self.on_clicked_avg15)
        self.average15x15.setObjectName("average15x15")
        
        self.gaussian3x3 = QtWidgets.QAction(MainWindow)
        self.gaussian3x3.triggered.connect(self.on_clicked_gaus3)
        self.gaussian3x3.setObjectName("gaussian3x3")
        
        self.gaussian5x5 = QtWidgets.QAction(MainWindow)
        self.gaussian5x5.triggered.connect(self.on_clicked_gaus5)
        self.gaussian5x5.setObjectName("gaussian5x5")
        
        self.gaussian7x7 = QtWidgets.QAction(MainWindow)
        self.gaussian7x7.triggered.connect(self.on_clicked_gaus7)
        self.gaussian7x7.setObjectName("gaussian7x7")
        
        self.gaussian9x9 = QtWidgets.QAction(MainWindow)
        self.gaussian9x9.triggered.connect(self.on_clicked_gaus9)
        self.gaussian9x9.setObjectName("gaussian9x9")
        
        self.gaussian11x11 = QtWidgets.QAction(MainWindow)
        self.gaussian11x11.triggered.connect(self.on_clicked_gaus11)
        self.gaussian11x11.setObjectName("gaussian11x11")
        
        self.gaussian13x13 = QtWidgets.QAction(MainWindow)
        self.gaussian13x13.triggered.connect(self.on_clicked_gaus13)
        self.gaussian13x13.setObjectName("gaussian13x13")
        
        self.gaussian15x15 = QtWidgets.QAction(MainWindow)
        self.gaussian15x15.triggered.connect(self.on_clicked_gaus15)
        self.gaussian15x15.setObjectName("gaussian15x15")
        
        self.median3x3 = QtWidgets.QAction(MainWindow)
        self.median3x3.triggered.connect(self.on_clicked_med3)
        self.median3x3.setObjectName("median3x3")
        
        self.median5x5 = QtWidgets.QAction(MainWindow)
        self.median5x5.triggered.connect(self.on_clicked_med5)
        self.median5x5.setObjectName("median5x5")
        
        self.median7x7 = QtWidgets.QAction(MainWindow)
        self.median7x7.triggered.connect(self.on_clicked_med7)
        self.median7x7.setObjectName("median7x7")
        
        self.median9x9 = QtWidgets.QAction(MainWindow)
        self.median9x9.triggered.connect(self.on_clicked_med9)
        self.median9x9.setObjectName("median9x9")
        
        self.median11x11 = QtWidgets.QAction(MainWindow)
        self.median11x11.triggered.connect(self.on_clicked_med11)
        self.median11x11.setObjectName("median11x11")
        
        self.median13x13 = QtWidgets.QAction(MainWindow)
        self.median13x13.triggered.connect(self.on_clicked_med13)
        self.median13x13.setObjectName("median13x13")
        
        self.median15x15 = QtWidgets.QAction(MainWindow)
        self.median15x15.triggered.connect(self.on_clicked_med15)
        self.median15x15.setObjectName("median15x15")
        
        self.rotate_right = QtWidgets.QAction(MainWindow)
        self.rotate_right.setObjectName("rotate_right")
        
        self.rotate_left = QtWidgets.QAction(MainWindow)
        self.rotate_left.setObjectName("rotate_left")
        
        self.scale_2x = QtWidgets.QAction(MainWindow)
        self.scale_2x.setObjectName("scale_2x")
        
        self.scale_1_2x = QtWidgets.QAction(MainWindow)
        self.scale_1_2x.setObjectName("scale_1_2x")
        
        self.translate_right = QtWidgets.QAction(MainWindow)
        self.translate_right.setObjectName("translate_right")
        
        self.translate_left = QtWidgets.QAction(MainWindow)
        self.translate_left.setObjectName("translate_left")
        
        self.actionsave = QtWidgets.QAction(MainWindow)
        self.actionsave.setObjectName("actionsave")
        
        self.menuFile.addAction(self.actionOpen)
        self.menuFile.addAction(self.actionSave_2)
        self.menuFile.addAction(self.actionExit)
       
        self.menuAverage_Filters.addAction(self.average3x3)
        self.menuAverage_Filters.addAction(self.average5x5)
        self.menuAverage_Filters.addAction(self.average7x7)
        self.menuAverage_Filters.addAction(self.average9x9)
        self.menuAverage_Filters.addAction(self.average11x11)
        self.menuAverage_Filters.addAction(self.average13x13)
        self.menuAverage_Filters.addAction(self.average15x15)
        self.menuGaussian_Filters.addAction(self.gaussian3x3)
        self.menuGaussian_Filters.addAction(self.gaussian5x5)
        self.menuGaussian_Filters.addAction(self.gaussian7x7)
        self.menuGaussian_Filters.addAction(self.gaussian9x9)
        self.menuGaussian_Filters.addAction(self.gaussian11x11)
        self.menuGaussian_Filters.addAction(self.gaussian13x13)
        self.menuGaussian_Filters.addAction(self.gaussian15x15)
        self.menuMedian_Filters.addAction(self.median3x3)
        self.menuMedian_Filters.addAction(self.median5x5)
        self.menuMedian_Filters.addAction(self.median7x7)
        self.menuMedian_Filters.addAction(self.median9x9)
        self.menuMedian_Filters.addAction(self.median11x11)
        self.menuMedian_Filters.addAction(self.median13x13)
        self.menuMedian_Filters.addAction(self.median15x15)
        self.menuFilters.addAction(self.menuAverage_Filters.menuAction())
        self.menuFilters.addAction(self.menuGaussian_Filters.menuAction())
        self.menuFilters.addAction(self.menuMedian_Filters.menuAction())
        self.menuRotate.addAction(self.rotate_right)
        self.menuRotate.addAction(self.rotate_left)
        self.menuScale.addAction(self.scale_2x)
        self.menuScale.addAction(self.scale_1_2x)
        self.menuTranslate.addAction(self.translate_right)
        self.menuTranslate.addAction(self.translate_left)
        self.menuGeometric_Transforms.addAction(self.menuRotate.menuAction())
        self.menuGeometric_Transforms.addAction(self.menuScale.menuAction())
        self.menuGeometric_Transforms.addAction(self.menuTranslate.menuAction())
        self.menuBar.addAction(self.menuFile.menuAction())
        self.menuBar.addAction(self.menuFilters.menuAction())
        self.menuBar.addAction(self.menuGeometric_Transforms.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuFilters.setTitle(_translate("MainWindow", "Filters"))
        self.menuAverage_Filters.setTitle(_translate("MainWindow", "Average Filters"))
        self.menuGaussian_Filters.setTitle(_translate("MainWindow", "Gaussian Filters"))
        self.menuMedian_Filters.setTitle(_translate("MainWindow", "Median Filters"))
        self.menuGeometric_Transforms.setTitle(_translate("MainWindow", "Geometric Transforms"))
        self.menuRotate.setTitle(_translate("MainWindow", "Rotate"))
        self.menuScale.setTitle(_translate("MainWindow", "Scale"))
        self.menuTranslate.setTitle(_translate("MainWindow", "Translate"))
        self.actionSave.setText(_translate("MainWindow", "Save"))
        self.actionOpen.setText(_translate("MainWindow", "Open"))
        self.actionSave_2.setText(_translate("MainWindow", "Save"))
        self.actionExit.setText(_translate("MainWindow", "Exit"))
        self.average3x3.setText(_translate("MainWindow", "3x3"))
        self.average5x5.setText(_translate("MainWindow", "5x5"))
        self.average7x7.setText(_translate("MainWindow", "7x7"))
        self.average9x9.setText(_translate("MainWindow", "9x9"))
        self.average11x11.setText(_translate("MainWindow", "11x11"))
        self.average13x13.setText(_translate("MainWindow", "13x13"))
        self.average15x15.setText(_translate("MainWindow", "15x15"))
        self.gaussian3x3.setText(_translate("MainWindow", "3x3"))
        self.gaussian5x5.setText(_translate("MainWindow", "5x5"))
        self.gaussian7x7.setText(_translate("MainWindow", "7x7"))
        self.gaussian9x9.setText(_translate("MainWindow", "9x9"))
        self.gaussian11x11.setText(_translate("MainWindow", "11x11"))
        self.gaussian13x13.setText(_translate("MainWindow", "13x13"))
        self.gaussian15x15.setText(_translate("MainWindow", "15x15"))
        self.median3x3.setText(_translate("MainWindow", "3x3"))
        self.median5x5.setText(_translate("MainWindow", "5x5"))
        self.median7x7.setText(_translate("MainWindow", "7x7"))
        self.median9x9.setText(_translate("MainWindow", "9x9"))
        self.median11x11.setText(_translate("MainWindow", "11x11"))
        self.median13x13.setText(_translate("MainWindow", "13x13"))
        self.median15x15.setText(_translate("MainWindow", "15x15"))
        self.rotate_right.setText(_translate("MainWindow", "Rotate 10 Degree Right"))
        self.rotate_left.setText(_translate("MainWindow", "Rotate 10 Degree Left"))
        self.scale_2x.setText(_translate("MainWindow", "2x"))
        self.scale_1_2x.setText(_translate("MainWindow", "1/2x"))
        self.translate_right.setText(_translate("MainWindow", "Right"))
        self.translate_left.setText(_translate("MainWindow", "Left"))
        self.actionsave.setText(_translate("MainWindow", "save"))
    
    


    def on_clicked_open(self):
        filename, _ = QtWidgets.QFileDialog.getOpenFileName(None,"QFileDialog.getOpenFileName()", "", "Images(*.png *.jpg *.jpeg)")
        if (filename):
            pixMap = QtGui.QPixmap(filename)
            self.image.setPixmap(pixMap)
            self.img = cv2.imread(filename)
    
    def on_clicked_avg3(self):
        print("on_clicked_avg3")
        
        self.averaged_img = AverageFilters(self.img, 3)
        self.averaged_img.average_filter()
        #result_image.jpg is created in AverageFilters class
        pixMap = QtGui.QPixmap("averaged3.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_avg5(self):
        print("on_clicked_avg5")
        
        self.averaged_img = AverageFilters(self.img, 5)
        self.averaged_img.average_filter()
        #result_image.jpg is created in AverageFilters class
        pixMap = QtGui.QPixmap("averaged5.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_avg7(self):
        print("on_clicked_avg7")
        
        self.averaged_img = AverageFilters(self.img, 7)
        self.averaged_img.average_filter()
        #result_image.jpg is created in AverageFilters class
        pixMap = QtGui.QPixmap("averaged7.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_avg9(self):
        print("on_clicked_avg9")
        
        self.averaged_img = AverageFilters(self.img, 9)
        self.averaged_img.average_filter()
        #result_image.jpg is created in AverageFilters class
        pixMap = QtGui.QPixmap("averaged9.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_avg11(self):
        print("on_clicked_avg11")
        
        self.averaged_img = AverageFilters(self.img, 11)
        self.averaged_img.average_filter()
        #result_image.jpg is created in AverageFilters class
        pixMap = QtGui.QPixmap("averaged11.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_avg13(self):
        print("on_clicked_avg13")
        
        self.averaged_img = AverageFilters(self.img, 13)
        self.averaged_img.average_filter()
        #result_image.jpg is created in AverageFilters class
        pixMap = QtGui.QPixmap("averaged13.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_avg15(self):
        print("on_clicked_avg15")
        
        self.averaged_img = AverageFilters(self.img, 15)
        self.averaged_img.average_filter()
        #result_image.jpg is created in AverageFilters class
        pixMap = QtGui.QPixmap("averaged15.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_gaus3(self):
        print("on_clicked_gaus3")

        self.gaussian_img = GaussianFilters(self.img, 3)
        self.gaussian_img.gaussian_filter()

        pixMap = QtGui.QPixmap("gaussian3.jpg")
        self.image.setPixmap(pixMap)
        
    def on_clicked_gaus5(self):
        print("on_clicked_gaus5")

        self.gaussian_img = GaussianFilters(self.img, 5)
        self.gaussian_img.gaussian_filter()

        pixMap = QtGui.QPixmap("gaussian5.jpg")
        self.image.setPixmap(pixMap)


    def on_clicked_gaus7(self):
        print("on_clicked_gaus7")

        self.gaussian_img = GaussianFilters(self.img, 7)
        self.gaussian_img.gaussian_filter()

        pixMap = QtGui.QPixmap("gaussian7.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_gaus9(self):
        print("on_clicked_gaus9")

        self.gaussian_img = GaussianFilters(self.img, 9)
        self.gaussian_img.gaussian_filter()

        pixMap = QtGui.QPixmap("gaussian9.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_gaus11(self):
        print("on_clicked_gaus11")

        self.gaussian_img = GaussianFilters(self.img, 11)
        self.gaussian_img.gaussian_filter()

        pixMap = QtGui.QPixmap("gaussian11.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_gaus13(self):
        print("on_clicked_gaus13")

        self.gaussian_img = GaussianFilters(self.img, 13)
        self.gaussian_img.gaussian_filter()

        pixMap = QtGui.QPixmap("gaussian13.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_gaus15(self):
        print("on_clicked_gaus15")

        self.gaussian_img = GaussianFilters(self.img, 15)
        self.gaussian_img.gaussian_filter()

        pixMap = QtGui.QPixmap("gaussian15.jpg")
        self.image.setPixmap(pixMap)
    
    def on_clicked_med3(self):
        print("on_clicked_med3")

        self.median_img = MedianFilters(self.img, 3)
        self.median_img.median_filter()

        pixMap = QtGui.QPixmap("median3.jpg")
        self.image.setPixmap(pixMap)


    def on_clicked_med5(self):
        print("on_clicked_med5")

        self.median_img = MedianFilters(self.img, 5)
        self.median_img.median_filter()

        pixMap = QtGui.QPixmap("median5.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_med7(self):
        print("on_clicked_med7")

        self.median_img = MedianFilters(self.img, 7)
        self.median_img.median_filter()

        pixMap = QtGui.QPixmap("median7.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_med9(self):
        print("on_clicked_med9")

        self.median_img = MedianFilters(self.img, 9)
        self.median_img.median_filter()

        pixMap = QtGui.QPixmap("median9.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_med11(self):
        print("on_clicked_med11")

        self.median_img = MedianFilters(self.img, 11)
        self.median_img.median_filter()

        pixMap = QtGui.QPixmap("median11.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_med13(self):
        print("on_clicked_med13")

        self.median_img = MedianFilters(self.img, 13)
        self.median_img.median_filter()

        pixMap = QtGui.QPixmap("median13.jpg")
        self.image.setPixmap(pixMap)

    def on_clicked_med15(self):
        print("on_clicked_med15")

        self.median_img = MedianFilters(self.img, 15)
        self.median_img.median_filter()

        pixMap = QtGui.QPixmap("median15.jpg")
        self.image.setPixmap(pixMap)
        

class App(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.show()

app = QtWidgets.QApplication(sys.argv)
window = App()
window.show()
sys.exit(app.exec_())
# def on_clicked_save(self):

# def on_clicked_exit(self):










# def on_clicked_rotate_r(self):

# def on_clicked_rotate_l(self):

# def on_clicked_scale2x(self):

# def on_clicked_scale1_2x(self):

# def on_clicked_translate_r(self):

# def on_clicked_translate_l(self):






import cv2
import math
import numpy as np
class GaussianFilters:
	def __init__(self, img, dimension):
		self.img = img
		self.dimension = dimension

	def create_kernel(self):
		kernel = np.zeros((self.dimension, self.dimension))
		dim = self.dimension // 2
		r = self.dimension - 1
		c = 0
		pi = 22/7
		sigma = 1 # I choose sigma as 1
		for x in range(-dim, dim+1):
			for y in range(-dim, dim+1):
				val = (1 / (2*pi*sigma))*math.exp(-( pow(x, 2) + pow(y,2) ) / (2*pow(sigma, 2)))
				kernel[r, c] = val
				r -= 1

			r = self.dimension - 1
			c += 1

		kernel_sum = 0
		for i in range(kernel.shape[0]):
			for j in range(kernel.shape[1]):
				kernel_sum += kernel[i,j]


		return kernel, kernel_sum

	def create_padded(self, ch):

		dim = self.dimension // 2	

		# print(ch)
		padded = np.zeros((ch.shape[0] + 2*(dim), ch.shape[1] + 2*(dim)))
		# print("padded shape:", padded.shape)
		# print("ch shape:", ch.shape)
		
		#FILLING THE CORNERS with the corner values of the relative channel of the image
		#top corner(s)
		for i in range(dim):
			#left corners
			for j in range(dim):
				padded[i, j] = ch[0,0]
			#right corners
			for k in range(padded.shape[1] - dim, padded.shape[1]):
				padded[i, k] = ch[0, ch.shape[1] - 1]

		#bottom corner(s)
		for i in range(padded.shape[0] - dim, padded.shape[0]):
			#left
			for j in range(dim):
				padded[i, j] = ch[ch.shape[0] - 1, 0]
			#right
			for k in range(padded.shape[1] - dim, padded.shape[1]):
				padded[i, k] = ch[ch.shape[0] - 1, ch.shape[1] - 1]

		#FILLING THE EDGES
		for k in range(dim, padded.shape[1] - dim):  #iterate over columns first
			#top rows
			for m in range(dim):
				padded[m, k] = ch[0, k - dim]
			#bottom rows
			for p in range(padded.shape[0] - dim, padded.shape[0]):
				padded[p, k] = ch[ch.shape[0] - 1, k - dim]


		#the most 
		for k in range(dim, padded.shape[0] - dim):
			#right row(s)
			for m in range(padded.shape[1] - dim, padded.shape[1]):
				padded[k, m] = ch[k - dim, ch.shape[1] - 1]
			#left row(s)
			for p in range(dim):
				padded[k, p] = ch[k - dim, 0]
			#the overlapped part of padded and the actual image
			for c in range(dim, padded.shape[1] - dim):
				padded[k, c] = ch[k - dim, c - dim] 

		return padded

	def response_ch(self, padded, ch, kernel, kernel_sum):
		res_ch = np.zeros((ch.shape[0], ch.shape[1]))
		total = 0 
		avg = 0 
		row = 0
		col = 0
		for x in range(padded.shape[0] - self.dimension + 1):
			for y in range(padded.shape[1] - self.dimension + 1):
				for i in range(self.dimension):
					for j in range(self.dimension):
						total += padded[x + i, y + j]*kernel[i,j]

				avg = round(total / (self.dimension*self.dimension))
				res_ch[row, col] = avg
				col += 1

			col = 0
			row += 1

		for i in range(res_ch.shape[0]):
			for j in range(res_ch.shape[1]):
				res_ch[i, j] = round(res_ch[i,j] / kernel_sum)
		
		return res_ch

	def gaussian_filter(self):
		b, g, r = cv2.split(self.img)

		kernel, kernel_sum = self.create_kernel()

		padded_b = self.create_padded(b)
		padded_g = self.create_padded(g)
		padded_r = self.create_padded(r)

		b = self.response_ch(padded_b, b, kernel, kernel_sum)
		g = self.response_ch(padded_g, g, kernel, kernel_sum)
		r = self.response_ch(padded_r, r, kernel, kernel_sum)
		
		b = np.uint8(b)
		g = np.uint8(g)
		r = np.uint8(r)
		image = cv2.merge((b, g, r))
		ext = str(self.dimension)
		cv2.imwrite("gaussian" + ext + ".jpg", image)








